# -*- coding: utf-8 -*-

from __future__ import print_function, division
from random import randint, random, choice


def get_random():
    i = randint(1, 3)

    if i == 1:
        # Return an integer between 1 and 1000
        return randint(1, 1000)
    elif i == 2:
        # Return a float number between 1 and 1000
        return random() * 1000
    else:
        # Return one choice of a list of options
        options = {
                'error': lambda: 'Error!',
                'hex': lambda: hex(randint(1, 1000))[1:],
                'octal': lambda: oct(randint(1, 1000)),
                'bin': lambda: bin(randint(1, 1000))[1:],
                }
        return options[choice(options.keys())]()


def main():
    with open("data/dataset", 'w') as fh:
        for i in range(10000):
            fh.write("{0}\n".format(get_random()))


if __name__ == "__main__":
    main()

# vim: ai sts=4 et sw=4
