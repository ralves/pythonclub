Oh my numbers!
==============

Welcome to your first exercise!

Purpose
-------

This exercise aims at practising the basics of **string processing** and
**numeric conversion** in context with **file manipulation**.

Problem
-------

A colleague :download:`sends you a file with results <../data/dataset>` but he forgot to compute the final sum.

The file should only contain numerical values but for some reason, some **text** is also present.

Your aim is to compute the **sum** of all the numbers in the file.

.. _003-solutions:

Solutions
---------

There are two possible solutions to this problem.
Start with the easiest one and if you are up to the challenge take the hard one too.

* **I know numbers 0 to 9** (*easier*) - Ignore lines which have at least **one** alphabetical character excluding **dots** (``123.456`` should be valid).

* **Binary, Octal and Hexadecimal are numbers too** (*harder*) - Ignore only lines with "**Error!**".

.. note::

    ``print()`` the result of your sum to get your solution.

Once you think you have found the solution type in your number in the address
bar ``003-oh_my_numbers-exercise/doc/solution/<your_solution>.html`` replacing
the ``<your solution>`` part with the number you found.

If your solution is incorrect you will get an error otherwise you will get a Congratulations page.

Hints
-----

.. note::

    This information is mostly relevant for the harder solution.

In the world one almost always uses numbers which are base **10**.
In base 10 you count like so::

    1 2 3 4 5 6 7 8 9 10 11 12 ...

However in computer world you frequently use other bases: **2** (binary), **8** (octal) and **16** (hexadecimal).

Counting in those bases works like so::

    binary      -> 1 10 11 100 101 110 111 1000 1001 1010 1011 1100 1101 1110 1111 10000 10001 10010 ...
    octal       -> 1  2  3   4   5   6   7   10   11   12   13   14   15   16   17    20    21    22 ...
    decimal     -> 1  2  3   4   5   6   7    8    9   10   11   12   13   14   15    16    17    18 ...
    hexadecimal -> 1  2  3   4   5   6   7    8    9   10    A    B    C    D    E     F    11    12 ...

To learn how to use different bases read the ``help`` of the ``int`` function.

.. note::

    Python has different textual representations for numbers in different bases.
    Use your searching skills to figure out what they are.

Member solutions
----------------

For future reference, here are a few solutions for this problem:

* Renato Alves - :download:`link <../solutions/rjalves.py>`
