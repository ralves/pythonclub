# -*- coding: utf-8 -*-

from __future__ import print_function, division


def easy():
    digits = '0123456789.'
    total = 0

    with open("data/dataset") as fh:
        for line in fh:
            # number is still at str() at this point
            number = line.rstrip('\n')
            is_a_number = True

            # Go
            for character in number:
                if character not in digits:
                    is_a_number = False
                    break

            if is_a_number:
                # Here use convert using the float function
                # it takes care of both integer and floating point numbers
                total += float(number)

    return total


def hard():
    total = 0

    with open("data/dataset") as fh:
        for line in fh:
            line = line.rstrip()

            # Takes care of floating point numbers
            if '.' in line:
                total += float(line)

            # Takes care of octal numbers
            elif line.startswith("0"):
                total += int(line, 8)

            # Takes care of hexadecimal numbers
            elif line.startswith("x"):
                total += int('0' + line, 16)

            # Takes care of binary numbers
            elif line.startswith("b"):
                total += int('0' + line, 2)

            # Takes care of anything which is an error (ignoring it by jumping
            # to the next line in the file)
            elif line.startswith("Error!"):
                continue

            # Takes care of integer numbers which are the leftovers
            else:
                total += int(line)

    return total


def easy_functional():
    from operator import add
    digit_check = lambda x: x[0].isdigit()
    return reduce(add, map(float, filter(digit_check, open("data/dataset"))))


def main():
    print("Easy solution:  ", easy())
    print("Hard solution:  ", hard())
    print("\nEasy functional:", easy_functional())


if __name__ == "__main__":
    main()

# vim: ai sts=4 et sw=4
