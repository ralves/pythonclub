*******************
Preparing workspace
*******************

Why Linux?
==========

Linux is a free (no need to buy it) and open (you can change it at will) *operative system*. In the old days it was ugly and difficult to use. Those days are long gone.

If you wish to use the common resources at the Institute, you will also need to know a little of Linux and that's what powers most servers.

Although *Python* is compatible with most platforms (Windows, Mac, Linux,...) we chose Linux in part because a lot of the community uses Linux but mostly because certain things are simply easier on Linux than on Windows or Mac.

VirtualBox
==========

In order to make the process of using Linux easier without interfering with what you have installed on your computer we will use a tool called **VirtualBox**.

*VirtualBox* is a software that creates an artificial *"virtual"* computer within your computer on which you can then install any system you would like.
If this idea is confusing for you, think of it as running a different *Operative System* (Windows, Mac, Linux,...) in a window as if it was any other program. 

Installing VirtualBox
---------------------

Just head over to the `VirtualBox website <https://www.virtualbox.org>`_ and download the *VirtualBox platform package* for your system (OS-X is for Mac users) and install it. If prompted accept all hardware creation messages.

Which Linux?
============

Due to its popularity we chose `Ubuntu Linux <http://www.ubuntu.com/>`_ and opted for a simpler version of it called `Xubuntu <http://xubuntu.org/>`_.

Installing Xubuntu
==================

.. note::
    During the Club session we installed Xubuntu by downloading a CD image (iso) and using it in VirtualBox.
    Because these instructions are extensive we opted for not including them here and instead provide a :ref:`ready-to-use-image`.

.. _ready-to-use-image:
    
Ready to use image
------------------

In order to use this image you will need at least 8GB of space on your disk and at least 1024MB or RAM (Memory).

You can download the image by `following this link <http://www.evocell.org/media/medialibrary/2012/10/PythonClub.ova>`_.

Configuring VirtualBox
----------------------

Once the download finishes (and assuming you already installed VirtualBox) you should have a file called ``PythonClub.ova``.

Open VirtualBox and in the menus navigate to :menuselection:`File --> Import Appliance`. Click ``Open appliance...`` and select the ``PythonClub.ova`` file. Then press ``Continue`` followed by ``Import``.

After a few minutes the system will be ready to be used.

Using VirtualBox
----------------

After importing there should be an entry in VirtualBox called ``PythonClub``. To use it simply select it and click the green arrow ``Start``.

A new window will open and after a few seconds/minutes you will be asked to login. The user and password are ``pythonclub``.

At this point you are done.

Better experience
^^^^^^^^^^^^^^^^^

For a better experience working with VirtualBox you can make the window *FullScreen* by pressing the ``Host Key`` and ``F``. The host key is indicated in the bottom right corner of the window. To exit FullScreen mode repeat the *FullScreen* command.

.. note::
    By default the ``Host Key`` is *right Control key* on Windows and Linux, and *left Command (cmd)* on Mac

Some bits of Linux
==================

Although learning Linux is beyond the scope of the Club, it will be useful to learn a few **commands** which will be used frequently in the **shell**.

The **Shell**, **Terminal** or **Console** is a command line interface to your system. Think of it as writing instructions instead of clicking things with the mouse. It may seem unproductive but once you get the hang of it you'll see how much easier it is to do some things.

The most important **commands** you need to know are ``cd`` (`change directories <http://linux.about.com/od/commands/a/Example-Uses-Of-The-Command-Cd.htm>`_) , ``ls`` (list content of a folder) and obviously ``python``.

You will learn a few more commands along the way. If you are in a rush here's a few of them ``pwd`` (know where you are), ``rm`` (remove files and folders), ``cp`` (copy files), ``mv`` (move and rename files), ``mkdir`` (create folders). 

If you want to know more about a certain command you can always run ``man`` (read the manual). For instance if you want to know more about the ``pwd`` command you can run ``man pwd``.
