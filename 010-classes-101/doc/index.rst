Classes 101
===========

Overview
--------

Using only functions in Python is fine for most small tasks. However, the moment you start reusing code and using external packages you will inevitably come across these structures.

So without further delay lets explain what they are and what they are used for.

How it looks like
-----------------

.. code-block:: python

    class MyStats(object):
        """MyStats handles simple statistical computations
        """
        def __init__(self, initial=None):
            self.data = initial or []
        
        def append(self, value):
            """Add more values to the dataset"""
            self.data.append(value)
          
        def sum(self):
            """Compute the sum of all numbers in the dataset"""
            if self.data:
                return sum(self.data)
            else:
                return None
         
        def mean(self):
            """Compute the mean of the dataset"""
            if self.data:
                return sum(self.data) / len(self.data)
            else:
                return None
           
        def median(self):
            """Compute the median of the dataset"""
            if self.data:
                data = sorted(self.data)
                size = len(data)
                middle = (size // 2) - 1
                
                if size % 2 == 0:
                    values = data[middle:middle + 2]
                    return sum(values) / 2
                else:
                    return data[middle]
            else:
                return None

Digesting it
------------

.. code-block:: python

    class MyStats(object):
        """MyStats handles simple statistical computations
        """
        def __init__(self, initial=None):
            self.data = initial or []

We define a Class by using the special keyword **class**.

Here the class *MyStats* is defined as inheriting the properties of **object**. In Python 2 always put **object** between parenthesis (more on this lower in the document).

It's followed by an optional string (called **docstring**) used for documentation purposes. It's also what is displayed when you do *help(MyStats)*.

We also define a special function called *__init__* which accepts two parameters: **self** and in this case an **initial** value which defaults to **None**.

The special function **__init__** is used to define parameters at initialization. If we don't have any parameter we would like to pass we don't need to define **__init__** at all.

An extra special parameter **self** represents an **instance** of this class. It is present both on the **__init__** line and on the first line of the function body. It should be present in any function defined inside the class and should always be the first argument.

.. note::
    The name *self* has no special meaning, but it became a convention (in other languages like JavaScript *this* is used instead of *self*).

The class defines a structure to hold data, much like a container. Because of this you can have several of this structures initialized with different datasets. Each of these is called an **instance** of a given class, in our case *MyStats*.

**Instances** are independent among each other. This means they can access properties of the *parent* (the class) but not of other instances.

Adding behavior
---------------

.. code-block:: python

     def append(self, value):
         """Add more values to the dataset"""
         self.data.append(value)
       
     def sum(self):
         """Compute the sum of all numbers in the dataset"""
         if self.data:
             return sum(self.data)
         else:
             return None

Here we list two types of functions. One that performs an action on the **instance** and returns nothing, and another which computes something with the data in the instance and returns it.

These functions are exactly like functions outside a class except they are passed by default a parameter **self**, which is actually the **instance**. This is what makes classes work the way they do.


How to use a class
------------------

Simply create an instance by calling the class::

    >>> dataset = MyStats()
    >>> dataset.append(5)
    >>> dataset.append(6)
    >>> dataset.append(7)
    >>> dataset.append(8)
    >>> dataset.sum()
    26
    >>> dataset.mean()
    6.5
    >>> dataset.median()
    6.5
    >>> dataset.append(8)
    >>> dataset.sum()
    34
    >>> dataset.mean()
    6.8
    >>> dataset.median()
    6

And because instances are independent, you can initialize multiple::

    >>> dataset1 = MyStats()
    >>> dataset2 = MyStats()
    # Dataset 1
    >>> dataset1.append(5)
    >>> dataset1.append(10)
    >>> dataset1.append(8)
    >>> dataset1.append(2)
    # Dataset 2
    >>> dataset2.append(1)
    >>> dataset2.append(2)
    >>> dataset2.append(3)
    >>> dataset2.append(4)
    # Statistics for both
    >>> dataset1.sum()
    25
    >>> dataset2.sum()
    10
    >>> dataset1.mean()
    6
    >>> dataset2.mean()
    2

Old vs New Style Classes
------------------------

In Python 2 you can define a class as::

    >>> class MyClass:
    ...     pass

or as::

    >>> class MyClass(object):
    ...     pass

The first is called **Old Style Class** while the latter is called **New Style Class**.

The difference between the two is not relevant and as standard practice you should always use **New Style Classes**

Warnings
--------

Don't initialize mutable objects (such as lists, dictionaries,...) directly on the function definition or they will be shared across all instances of the same class.

In other words, this is correct::

    def __init__(self, initial=None):
        self.data = initial or []

but this is not::

    def __init__(self, initial=[]):
        self.data = initial

If you used the second case, this would happen::

    >>> dataset1 = MyStats()
    >>> dataset2 = MyStats()
    # Dataset 1
    >>> dataset1.append(5)
    >>> dataset1.append(10)
    >>> dataset1.append(8)
    >>> dataset1.append(2)
    # Nothing was added to Dataset 2, but
    >>> dataset1.sum()
    25
    >>> dataset2.sum()
    25  # because dataset2.data is the same as dataset1.data
    >>> dataset1.mean()
    6
    >>> dataset2.mean()
    6
