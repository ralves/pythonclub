#!/bin/bash
# autohtmlbuilder.sh -- created 2012-09-11, Renato Alves
# @Last Change: 11-Sep-2012.
# @Revision:    0.1
#
# Use this script to rebuild html pages every time a file is changed
# This script requires inotifywait from the inotify-tools package

command -v inotifywait >/dev/null 2>&1 || { echo >&2 "Command inotifywait not found, please install inotify-tools. Exiting."; exit 1; }

MONITORDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

while inotifywait -r --exclude $MONITORDIR/.git --exclude $MONITORDIR/_sphinx_build -e modify -e move -e create -e delete $MONITORDIR >/dev/null 2>&1; do
    echo $line
    make html
done

# vi: 
