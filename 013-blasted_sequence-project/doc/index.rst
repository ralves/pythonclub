.. _Project-Part-2:

Blasted sequences
=================

Purpose
-------

In this second part we will use BLAST to look for and retrieve similar sequences.

Description
-----------

In the third part we will be testing if we can use restriction enzymes to tell
apart different strains of *Escherichia coli*.

Inorder to retrieve fimA sequences from different strains of *E. coli*,
we will be using BLAST with a high threshold (low e-value).

This is a quick and dirty approach, because even though BLAST returns a
reference to the full sequence (the accession id), we are only interested in
the region that has high similarity with our sequence.

.. note::
    Because BLAST relies on similarity it is likely that the matching regions
    are not always of the same size as the original sequence. For the purpose
    of this project this will not be a problem.

Problem
-------

Using the *fimA* sequence that you obtained in the :ref:`first part
<Project-Part-1>` of the project you should now write a small script that runs
the sequence against NCBI's **nr** database.

Additionally you should filter the output to include only *E. coli* sequences.

Store the first 50 hits as a FASTA formatted file. Use the first **HSP** for
each sequence. (Check hints for explanation on HSPs).

In addition to the description of the hit provided by BLAST, make sure the name
of the gene (fimA) is present in the header of each sequence.
If it's not, add it yourself programatically.

If later on we need to move sequences around, we want to know both the sequence name,
as well as where it came from.

Data
----

Use the data obtained in :ref:`part one <Project-Part-1>`.

Hints
-----

All the needed tools are available in BioPython.

You will need to use the modules *Bio.Blast.NCBIWWW* and *Bio.Blast.NCBIXML* to
run BLAST and parse the output.

In order to only have *Escherichia coli* sequences as BLAST results, pass
*"Escherichia coli[orgn]"* to parameter **entrez_query** in *NCBIWWW.qblast*.

You will need *Bio.SeqRecord.SeqRecord* and *Bio.Seq.Seq* to create objects
which you can then pass to *Bio.SeqIO* to write to a file in FASTA format.

If you can't make sense of the output from *Bio.Blast.NCBIXML* know that for
each input sequence BLAST will return one or more **alignments** against different
sequences and for each alignment one or more **high-scoring segment pairs** (HSP).

Think of an **HSP** as a region of two sequences that aligns without major
gaps. Aligning 2 two-domain proteins could result in an alignment with 2 HSPs
if the region between the 2 domains aligns poorly.
