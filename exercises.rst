.. Python Club documentation master file, created by
   sphinx-quickstart on Fri Sep  7 18:32:12 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Exercises
=========

You should be able to do these exercises without any prior. If you feel lost
while doing them, check the previous (non exercise) sessions for extra
information.

These exercises are independent of each other and don't have to be solved by
the order they are given.

.. toctree::
    :maxdepth: 2
    :glob:
    :numbered:

    *-exercise/doc/index
