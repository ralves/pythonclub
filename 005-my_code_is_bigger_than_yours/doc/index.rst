My code is bigger than yours - writing reusable code
====================================================

Code re-usability?
------------------

What?
^^^^^

When programming, having lots of code means things can go wrong is more places.
So **more** doesn't necessarily mean **better**.

This is specially problematic if you repeat things or copy-paste a lot.
Copy-paste is something you definitely want to avoid at all costs.

Re-usable code is as the name implies, something you can use in different
places and contexts.

Why?
^^^^

Imagine that you have a block of code that does a certain action, say save a
list of results to a file. If you copied this block to several places and later
realize you did a mistake, you will need to correct all those places.

Thinking about using find/replace to fix something may also not be the best
choice cause things may be accidentally altered without being noticed.

Re-using code means that your trust on it is bigger since you have been using
it for a longer time. It also means that if you find something wrong you know
which parts of the work you did in the past may have been affected.

How?
^^^^

Python provides four basic structures to allow easy re-use of code:

* **packages** - a container for modules.
* **modules** - a container for classes and functions
* **classes** - a collection of functions restricted to a certain use
* **functions** - the basic re-usable code unit

We'll go over them in detail later on and we'll leave **classes** to the end.
These are slighly more advanced and require additional notions which we haven't
covered yet.

Overview
--------

.. _Naming_restrictions:

Naming restrictions
^^^^^^^^^^^^^^^^^^^

In Python and many other languages, you are not allowed to use any names you can think of.

The restriction is there both for sanity and readability reasons and comes all the way up from **variables**.

In case you haven't come across this, you are not allowed to use the following variable names:

    >>> # you can't do this:
    >>> 1element = 1
    >>> -name = 'you'
    >>> my mice = ['Pinky', 'Brain']

These translate to the following:

* Variable names can be composed of latin alphabetic characters: **a-z**,
  **A-Z**
* Special characters are not allowed, white space is considered a special
  character: **.,="'*!? {}[]()£#;:çááâã** (just to list a few)
* Numbers are only valid if they are not the first character: **me2** is valid,
  **2me** is not
* Underscores are allowed (they have a special *convention* meaning when used
  as first character): **My_two_names** and **_not_me** are valid

If you don't want to memorize all that, make your life easier by using only
**a** to **z**, lower or upper-case and underscore **_**

As you might have guessed, these limitations also apply to **package**,
**module**, **classes** and **functions** because they need to have a **name**
if you want to use them inside Python.

Package
-------

A **package** is basically a folder. In order to be distinguished from other
folders it must contain the special file **__init__.py**. This file is empty in
most cases but should always exist if you want Python to recognize the folder
as a **package**.

An important note to keep from the :ref:`Naming_restrictions` section is
**don't use spaces on your folder names, use underscores instead**.

Module
------

A **module** is basically a file following the same naming restrictions from before.
The filename should end with the **.py** extension.
It usually exists within *packages* but it is not mandatory.

Anything you write inside a *module* can technically be re-used.

At execution time it can be distinguished from a regular script by inspecting
the content of the special variable **__name__**. (thats *two* underscores before,
and *two* after).

If it is a script **__name__** will contain the string *'__main__'* (also
with *two* and *two* underscores). If it's a
module it will contain the **name** of the file, without the *.py* part.

.. note::
    The word *script* is used here as a synonym of the action `python myfile.py`.
    The word *module* is used here as a synonym of the action `import myfile`.

This behavior is frequently used to make a file work both as **script** and
**module** by including the follwing lines at the end of the file:

    >>> if __name__ == "__main__":
    ...     do_something_only_as_script()

Function
--------

This is the unit you will use on a regular basis. It is something you can write
in a *module*.

A function typically looks like:

    >>> def myfunction(arg):
    ...     do_something...

the keyword **def** is used to *define* a function. After being defined you can
call it by doing:

    >>> myfunction(some_argument)

and whatever was defined in the function will be executed with the given
arguments as input.


Definition
^^^^^^^^^^

A function may accept no arguments, a fixed number of arguments or an unlimited
number of arguments (in which case they can be named, or unnamed).

You can also define a default value for arguments, which is used in case of
omission.

These definitions look like so:

* *No arguments*::
    >>> def myfunction():
    ...     ...

* *Fixed number of arguments*::
    >>> def myfunction(arg1, arg2):
    ...     ...

* *Fixed number of arguments with default values*::
    >>> def myfunction(arg1=2, arg2="Some text"):
    ...     ...

* *Arbitrary number of unnamed arguments*::
    >>> def myfunction(*args):
    ...     # args will be a list
    ...     ...

* *Arbitrary number of named arguments*::
    >>> def myfunction(**kwargs):
    ...     # kwargs will be a dictionary
    ...     ...

* *All from before*::
    >>> def myfunction(arg1, arg2="Something", *args, **kwargs):
    ...     ...

Usage
^^^^^

When calling a function it's arguments can be passed in two different ways. So
given the function:

    >>> def myfunction(arg1, arg2, arg3):
    ...     do_something...

You could call this function by passing:

* *Unnamed* arguments::

    >>> myfunction('something', 42, 'something else')

    # or if you already have a list with the three elements ::

    >>> mylist = ['something', 42, 'something else']
    >>> myfunction(*mylist)

* *Named* arguments::

    >>> myfunction(arg3='something else', arg1='something', arg2=42)

    # or if you already have a dictionary with the three elements ::

    >>> mydict = {'arg3': 'something else', 'arg1': 'something', 'arg2': 42}
    >>> myfunction(**mydict)

Functions can also return values, using the *return* keyword::

    >>> # a pointless function
    >>> def mysum(x, y):
    ...     return x + y
    ...
    >>> s = mysum(1,2)
    >>> s
    3


Re-using code from modules and packages
---------------------------------------

Having all the trouble of creating functions and modules wouldn't make any
sense if you could not use them outside the same script where they were
defined. To deal with this, Python has the keyword **import ...** or **from ...
import ...**

Assuming you have a *package* called **pythonclub** on the same folder where
you are writing a new script. Since it's a package it must contain the
*__init__.py* file.
Next to the *__init__.py* file there's also a **module** called *functions.py*
and inside it a **function** called *square* that accepts one numeric
*argument* and returns it's square number.

So in your script, you could use it like so::

    >>> from pythonclub.functions import square
    >>> square(5)
    25

If by some accident you already had a variable named **square** in your code
you could instead do::

    >>> from pythonclub import functions
    >>> functions.square(5)
    25

or even, if you didn't have the **package** *pythonclub* but instead only had
the file *functions.py* on the same folder as your script you could simply do::

    >>> import functions
    >>> functions.square(5)
    25

Class
-----

When you start having lots of functions, it becomes difficult to keep track of
what functions deal with what types of data. You may have a function that
accepts a list and saves it to a file, and some other function that does
something very similar but requires a dictionary instead. Or you may have
functions which only make sense on DNA and not Protein or other types of
sequence.

Classes help you organize functions in a way that they are kept into one place
that provides a context.

However since they allow you to do a lot more than that, we'll cover this topic
in a later session.

Just to kill the curiosity, this is what a class could look like::

    >>> class DNA(object):
    ...     def __init__(self, sequence, codon_table=None):
    ...         self.seq = sequence
    ...         if codon is None:
    ...             self.codon = DEFAULT_CODON_TABLE
    ...         else:
    ...             self.codon = codon_table
    ...     def translate(self):
    ...         """
    ...         Translates DNA to Protein using the given codon table and
    ...         return a Protein object
    ...         """
    ...         translation = ...
    ...         return Protein(translation)

