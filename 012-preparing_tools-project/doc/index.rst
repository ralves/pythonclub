.. _Project-Part-1:

Preparing tools
===============

Purpose
-------

While training the use of Python features such as *modules*, *classes* and
*functions* you will create a sequence of small scripts that will be reused on
subsequent **Project** exercises.

.. note::
    The amount of files and folders in project will grow with time. It will be
    good to have a folder structure that helps you keep things organized.

Description
-----------

Every time one starts a new project the first step is to make a rough plan of
what experiments will be performed and what materials and tools will be needed.

For the "what experiments" part you are still not entirely sure about all the
things you will do... it will largely depend on the results you will get along
the way.

Even so, you are positive that you will need to deal with sequences (both DNA
and Protein) so maybe having a quick way of downloading these from a database
would be a good thing to have.

You are also thinking that eventually you will need to have some pretty way of
showing results, but for now it also falls on the category of "I have no idea
what the results will look like".

Problem
-------

In this block we will start by writing code to retrieve *Protein* and *DNA*
sequences from a set of Gene/Protein identifiers from two resources,
`Uniprot <http://www.uniprot.org>`_ and `GenBank <http://www.ncbi.nlm.nih.gov/genbank/>`_.

You will also need to write code to read the provided format and write any
results in whatever format suits you best.

.. note::
    When storing plain sequences, `FASTA <http://en.wikipedia.org/wiki/FASTA_format>`_
    is one of the most widely used formats

Also try to organize files automatically into different folders depending on
what they are.

.. note::
    Avoid using spaces in filenames, use underscores **_** instead.

Data
----

The :download:`data file <../data/genes>` contains 2 genes from 2 different
species.

Hints
-----

`BioPython <http://biopython.org/DIST/docs/tutorial/Tutorial.html>`_ provides all of the functionality needed for this block.

Have a look at *Bio.ExPASy* and *Bio.GenBank* for retrieving sequences and
*Bio.SeqIO* to read and write different formats.

Things to come
--------------

* Working with restriction enzymes, chose the right one for the task.
* Old school SNP detection.
* Primer design
