# -*- coding: utf-8 -*-

from __future__ import print_function, division
from Bio import ExPASy, SeqIO
from StringIO import StringIO
import csv

class DataPoint(object):
    """ A simple class to house 
    """
    def __init__(self, gene_name, identifier, id_type, sequence_type,
                 species_name, ncbi_taxon_id ):

        self.gene_name = gene_name
        self.identifier = identifier
        self.id_type = id_type
        self.sequence_type = sequence_type
        self.species_name = species_name
        self.ncbi_taxon_id = ncbi_taxon_id

    def fetch_genbank(self):
        """ set self.fasta to the FASTA sequence corresponding to
        self.identifier fetched from NCBI/GenBank.
        """

    def fetch_uniprot(self):
        """ set self.fasta to the FASTA sequence corresponding to
        self.identifier fetched from NCBI/GenBank.
        """
        handle = ExPASy.get_sprot_raw(self.identifier)
        return SeqIO.read(handle, 'swiss')

    def fetch_record(self):
        if self.id_type == 'Uniprot (SwissProt)':
            return self.fetch_uniprot()
        elif self.id_type == 'NCBI (Gene ID)':
            return self.fetch_genbank()
        else:
            print("Unknown id_type: {0}".format(self.id_type))


class FastaGenerator(object):
    """ A class to parse data from an input file, and create appropriate output
    files in FASTA format.
    """
    def __init__(self, infile):
        self.infile = infile
        self.dna_records = []
        self.protein_records = []

    def parse_input(self):
        """ Parse input file.
        """
        o = open(self.infile)
        fields = o.readline().strip().split('\t')
        reader = csv.DictReader(o, fieldnames=fields, delimiter='\t')
        for entry in reader:
            data_point = DataPoint(
                gene_name = entry['Gene Name'],
                identifier = entry['Identifier'],
                id_type = entry['Identifier type'],
                sequence_type = entry['Sequence type'],
                species_name = entry['Species name'],
                ncbi_taxon_id = entry['NCBI Taxonomy ID'],
            )
            if data_point.sequence_type == 'DNA':
                self.dna_records.append(data_point.fetch_record())
            elif data_point.sequence_type == 'Protein':
                self.protein_records.append(data_point.fetch_record())
            else:
                print("Unknown sequence_type {0}".format(data_point.sequence_type))

        o.close()

    def write_protein_fasta(self,outfile):

        o = open(outfile,'w')
        SeqIO.write(self.protein_records, o, format='fasta')
        o.close()

def main():

    fg = FastaGenerator('../data/genes')
    fg.parse_input()
    fg.write_protein_fasta('../data/protein.fasta')


if __name__ == "__main__":
    main()

# vim: ai sts=4 et sw=4
