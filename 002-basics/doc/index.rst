*************
Python Basics
*************

Welcome to python!
==================

Python is a great programming language, especially for those new to programming.
Its focus is on clarity of writing and especially **readability**. It's one of the easiest
languages to learn, and has practically no limitations to what you can do with it,
other than those imposed by your imagination (and skill).

A little background
-------------------

Python is an open-source programming language developed by a dynamic community 
of programmers working and contributing to the greater good of the wonderful world
that is python. Due to its open-source-ness, there are thousands
of community contributed packages that you can get a hold of and use,
all for free.

Although the open-source nature of python means that there is very little
centralized coordination of developments in python, there is one person worth
mentioning, the **BDFL** (benevolent dictator for life): Guido van Rossum.

.. image:: guido.jpg

Guido was the creator of Python back in the 90's, and is still the person
responsible for all major decisions in the python community.

Using python
------------

Python is an **interpreted** programming language. Don't worry
about what that actually means. But this gives it one great
advantage over many other programming languages: you can
use it interactively.

Try typing ``python`` (followed by enter) in the command line, 
and you will be greeted with a friendly python prompt::

	>>>

You can immediately start talking in python::

	>>> print('hello world!')

followed by 'Enter', and python will gladly do as instructed,
and print::

	hello world!

into the interactive python session. You can exit by typing::

	>>> quit()

Additionally, you can also write and store your python scripts and programs
into a text file, and run them from the command line. Create a file
called ``hello.py`` with the following contents::

	print('hello world! I am a python file!')

Save the file, and in the same folder as the file, type into the command line::

	$ python hello.py

And you will be greeted with::
	
	hello world! I am a python file!

Summary
-------

If you want to write anything you want to keep,
save it as a ``*.py`` file. But, if you ever want
to know whats going on, fire up an interactive python
session, and just try it out! This is great for debugging
and testing small fragments of code.
And remember, Google is your best friend.


Some basic programming and python 
=================================

Before you get started with python there are some basic (general
programming) things you'll need to get familiar with.
Its not all intuitive, but knowing why '1' is something different than 1
is important. So let's talk about **data types**.


About numbers, text and other data types
----------------------------------------

Python, like most programming languages, works with different
types of data, or **data types**. There are a number of different
**data types** which are commonly used:

* **int**: integers
* **str**: strings
* **float**: floating point (decimal) numbers
* **bool**: boolean (True or False)

Each one of these types as an associated function with the same
name, which can be used to declare and convert between types
when possible::
    
    >>> a = int(1)
    >>> b = 1
    >>> a == b
    True
    >>> a = 1         # an integer
    >>> x = '1'       # a string
    >>> a == x
    False
    >>> b = int(x)    # convert '1' to 1
    >>> a == b
    True

.. note::
    If you want to find out what data type a variable
    is, python has a built-in ``type`` function::

        >>> a = 1
        >>> b = '1'
        >>> type(a)
        <type 'int'>
        >>> type(b)
        <type 'str'>


All about quotation marks...
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Python is pretty smart when it comes to handling strings. You can
define them using either " or ', just start and end with the same one::

    >>> a = 'some string'
    >>> a                   # short way to print something in python (interactive mode only)
    'some string'
    >>> b = "another string"
    >>> b
    'another string'
    >>> c = "this way you can use 'single quotes' in your text"
    >>> c
    "this way you can use 'quotation marks' in your text"
    >>> d = 'it works the "other way" to'
    >>> d
    'it works the "other way" to'

.. note::
    For assigning variable names, the spaces around the '=' are not
    required::

        >>> a = 1       # the most used and accepted way
        >>> a= 1        # this works
        >>> a =1        # so does this
        >>> a=1         # as does this
        >>> a    =   1  # and this is just stupid

    But, for the sake of style and **readability**, ``a = 1`` is 
    the preferred way.


Comments
--------

Comments are a great way to explain your code, and
writing proper documentation and comments will save you
a lot of hassle when you revisit old code, or try to share
it with someone::

    # using '#' for a comment 

They are never read by python::

    >>> a = 1
    >>> # a = 2
    >>> a
    1
    >>> # this should not surprise you, right?


Meet the Operators
------------------

Once you've become comfortable with the different data types,
its time to have fun with **operators**. We've already seen two
of them: ``=`` and ``==``. This are the most commonly used ones:

* **+ , - , * , / , \*\* and %**: for numerical calculations
* **+ and ***: for strings
* **=** : for setting the value of a variable
* **== , < , <= , > and >=** for comparisons

Operations on numbers
^^^^^^^^^^^^^^^^^^^^^

First, lets add some numbers::
    
    >>> 1 + 2    # lets keep it simple
    3
    >>> a = 1 + 2 
    >>> a
    3
    >>> b = 4
    >>> c = a + b
    >>> c
    7

And similarly subtraction (-), multiplication (*) and division (/).

.. note::
    Try finding the value of 7/3. This might not be what you expected.
    This is because in python version 2.x, if you apply **/** to two
    integers, python will return an integer. Just keep this in mind.

The other two numerical operators are::
    
    >>> 2 ** 3    # 2 to the power of 3
    8
    >>> 10 % 3    # 10 modulus 3 (remainder after division)
    3


Operations on strings
^^^^^^^^^^^^^^^^^^^^^

There are two operations that work on strings::

    >>> a = 'word'
    >>> b = a + 's'     # add 's' to the end
    >>> b
    'words'
    >>> c = b * 3       # 'times' 3
    >>> c
    'wordswordswords'


Comparing stuff
^^^^^^^^^^^^^^^

The comparison operators are the ones typically used in normal maths,
and always return **True** or **False** (boolean values, remember?)::

    >>> 3 > 2   # is 3 greater than 2?
    True
    >>> 3 > 3   # is 3 greater than 3?
    False
    >>> 3 >= 3  # is 3 greater than or equal to2?
    True
    >>> 3 == 3  # and of course, 3 equals 3? 
    True


Sequences
=========

Python knows multiple ways of storing sequences, the
most common of which are **sets**, **lists** and **tuples**.

    >>> a = [1,2,3]    # simple list
    >>> a
    [1,2,3]
    >>> type(a)
    <type 'list'>

The differences between the three are as follows:

* **lists** are **ordered**, can be **edited** and may **repeat**
* **sets**  are **unordered**, can be **edited**, and are **unique**
* **tuples** are **ordered**, are **uneditable**, and may **repeat**

.. note::
    The main difference between lists and sets vs. tuples is that the first
    two can be **edited** after you first declare them. This is referred to
    as **mutable** or **immutable** in python. Don't worry to much about that
    for now.

Similar to **int**, **str** and **float**, python has functions to declare
and convert between structures::
    
    >>> a = [3,2,1,1]  # a simple list
    >>> b = set(a)     # convert to a set
    >>> type(b)
    <type 'set'>
    >>> b              
    set([1,2,3])       # unordered and unique
    >>> c = tuple(b)
    >>> c
    (1, 2, 3)

Accessing sequence elements
---------------------------

You can access elements of lists and tuples by supplying an index::

    >>> a = [6,7,8]    # a simple list
    >>> a[1]
    7

.. warning::
    Python starts counting at **zero**, remember this when accessing elements
    in a list or tuple!!!

You can also access parts of a list or tuple using slices::

    >>> a = (6,7,8,9,10)    # a simple tuple
    >>> a[2:4]              # access elements 2 up to but not including 4
    [8, 9]
    >>> a[2:]               # access elements 2 until the end
    [8, 9, 10]
    >>> a[:3]               # access all elements up to but not including 3
    (6, 7, 8)
    >>> a[-1]               # negative indexes work in reverse
    10

.. note::
    What happens when you try to access a **set** using indexing?

You can also directly compare lists, tuples and sets in python::

    >>> a = [1,2,3]
    >>> b = [1,2,3]
    >>> a == b
    True

But pay attention. Just with comparing integers to strings, lists,
tuples and sets are different objects::

    >>> a = [1,2,3]
    >>> b = (1,2,3)
    >>> a == b
    False

.. note::
    Pay attention to trailing commas!

        >>> a = 1
        >>> b = 1,   # see the comma?
        >>> a == b
        False
        >>> a        # this is an integer
        1
        >>> b        # and this is a tuple
        (1,)


By using the indexes in lists, you can also manipulate values::

    >>> a = [1,7,3]   # that's not right
    >>> a
    [1, 7, 3]
    >>> a[1] = 2      # let's fix that...
    >>> a
    [1, 2, 3]

More complex operations on lists can be done with functions
such as ``append``, ``insert``, ``pop`` and ``remove``::

    >>> a = [1,2,3,4]
    >>> a.append(5)      # append number 5 to the end of the list
    >>> a
    [1, 2, 3, 4, 5]

.. note::
    Notice how the number 5 was appended directly to ``a``?. We didn't
    have to define a new variable::

        >>> b = a.append(5) # WRONG!

``insert`` can be used to insert a value anywhere in the list, and
elements can be removed with ``remove``::

        >>> a = [8, 6, 2]
        >>> a.insert(2,4)    # insert at position 2 value 4
        >>> a
        [8, 6, 4, 2]
        >>> a.remove(8)      # remove the value 8 from the list
        >>> a
        [6, 4, 2]

And lastly you can also remove a value, and return it using ``pop``::
    
        >>> a = [8,6,4,2]
        >>> a.pop(1)       # remove the value at position 1
        6
        >>> a
        [8, 4, 2]
        >>> b = a.pop(0)   # let's do that again, but store the 'popped' number
        >>> a
        [4, 2]
        >>> b
        8

.. note::
    There are some more functions you can use on lists, in a similar way:
    ``count``, ``sort`` and ``reverse``. Try to use python's built in ``help``
    function to find out what they do::
        
        >>> a = [1,2,3] 
        >>> help(a.count)
        >>> help(list.count)   # also works
        >>> # But, usually google is better :)
        

Dictionaries  
============

Dictionaries are an extremely useful way to store data. Each entry
has two components: a lookup **key** and a **value**. Think of it
like a real dictionary: Each 'entry' is a word (key), which has a description
(value). You can use them as follows::

    >>> b = {'some_key' : 5}      # pay attention, use curly brackets and a colon!!!
    >>> b['some_key']          # lookup entry with key = 'some_key' (this time, square brackets!) 
    5

We have now created a dictionary with one entry. This entry has key='some_key' and value=5.
We can add more entries, separated by commas::

    >>> b = {'some_key' : 12345, 'one_more': 99999} 
    >>> b.keys()                    # gives us a list of the keys.
    ['some_key', 'one_more']
    >>> b['some_key']               # lookup entry with key='some_key'
    12345
    >>> b['one_more']               # lookup entry with key='one_more'
    99999
    >>> b['one_more'] = 12121       # change the value of an entry...
    >>> b['one_more']               # lookup entry with key='one_more'
    12121
    >>> b['and_moar'] = 1337        # and add more key/value pairs to an existing dictionary
    >>> b.keys()
    ['some_key', 'one_more', 'and_moar']
    >>> b.values()                  # Will give us the values.
    [12345, 12121, 1337]
    >>> b.pop('some_key')           # To remove a key/value pair, and return the value
    12345

.. note::

    Dictionaries are **not sorted**. They do not have a 'first' or 'last' key. So, you
    cannot access an entry by 'position' or 'index'.

Conditional Statements
======================

Conditional statements lie at the core of most programming languages. They
allow your code to make decisions based on the values of one or more variables. The 
most frequently used conditional is the **if** statement::
    
    >>> a = 5
    >>> if a > 4:
    ...    print('a is greater than 4')
    ...
    a is greater than 4

.. note::
    Python uses indentation to separate logical blocks of code. Other languages
    use parenthesis, brackets, or some other type of demarcation. The **':'** is 
    added to the end of every line that is followed by an indented block.

if ... (elif) ... else ...
--------------------------
The simple **if** **else** combination will always do one of two things,
depending on the result of the **if** statement::

    >>> a = 2
    >>> if a == 1:            # remember '=' vs. '=='
    ...    print('a is one')
    >>> else:
    ...    print('a is NOT one')
    ...
    'a is NOT one'

The **elif** statement (short for "else, if...") allows you to chain multiple
**if/else** blocks::

    >>> a = 2
    >>> if a == 1:
    ...    print('a is one')
    >>> elif a == 2:
    ...    print('a is two')
    >>> else:
    ...    print('a is NOT one or two')
    ...
    'a is two'

.. note::
    
    if ... elif .. else sequences are evaluated as you would logically expect. Read them
    like a sentence. What would the difference be between the output of these two pieces
    of code?::

        >>> # First example
        >>> a = 1
        >>> if a > 0:
        ...     print('a is greater than 0')
        >>> elif a < 2:
        ...     print('a is less than 2')
        ...
        ???

        >>> # Second example
        >>> a = 1
        >>> if a > 0:
        ...    print('a is greater than 0')
        >>> if a < 2:
        ...    print('a is less than 2')
        ...
        ???

if ... (elif) ... else ... (more complex examples)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Multiple expressions can be combined for each **if** and **elif** statement
using **and** and **or**::

    >>> if (a > 0) and (b > 0):
    ...         # do something
    >>> elif (a > 0) or (b < 0):
    ...         # do something else
    ...

In the above example, the parenthesis are not necessary. But, it's good practice
to keep your code readable.

.. note::
    The **if** statement and it's friends basically test whether the statement that
    follows is results in True. The above example can be understood as:

    >>> a = 1
    >>> b == 0
    >>> if (True) and (False):   # evaluates to False
    ...         # do something
    >>> elif (True) or (False):  # evaluates to True
    ...         # do something else
    ...

There is also a shortcut for looking up values in lists (also works for
tuples and dictionaries)::
    
    >>> a = [1, 2, 3]
    >>> if 2 in a:
    ...    print('number 2 is in the list!!!')

In the case of dictionaries, the **in** operator only looks at the
**keys** of the dictionary::

    >>> a = {'one':1, 'two':2}
    >>> if 2 in a:
    ...    print('number 2 is in the dictionary!!!')
    >>> if 'two' in a:
    ...    print('the word "two" is in the dictionary!!! Its value is:')
    ...    print(a['two'])
    the word "two" is in the dictionary!!! Its value is:
    2

Loops: for and while
--------------------
There are two ways to create loops in python; using ``while`` and using
``for``::

    >>> for i in [1, 3, 4, 6]:
    ...     print(i)
    ...
    1
    3
    4
    6

    >>> a = 0
    >>> while a < 5:
    ...     a = a + 1
    ...     print(a)
    ...
    >>> print('done!')
    1
    2
    3
    4
    5
    done!

.. note::
    
    Notice how indentation is once again used to separate logical blocks of code. 

Typically, if you have a list, tuple or dictionary who's values you want to loop
through, you will end up using a ``for`` loop. When looping over a dictionary,
you will loop through the **keys** of the dictionary::

    >>> a = {'one':1, 'two':2}
    >>> for k in a:
    ...      print('next key/value pair')
    ...      print('key:')
    ...      print(k)
    ...      print('has value:')
    ...      print(a[k])
    ...

Or, a more elegant example, similar to the one above, but using string concatenation::

    >>> a = {'one':1, 'two':2}
    >>> for k in a:
    ...      output = 'key: ' + k + ' has value: ' + str(a[k])
    ...

.. note::
    
    The ``for`` statement can be used to loop through all elements in a list, but
    also to loop through all characters in a string. What will the output of the
    following two examples be?::

        >>> # first example
        >>> a = ['text',]
        >>> for i in a:
        ...     print(i)
        ...
        ???

        >>> # second example
        >>> a = 'text'
        >>> for i in a:
        ...     print(i)
        ...
        ???

Dealing with files
==================

In Python much like any programming language, **reading from** and **writing to** files
is something you do quite frequently.

You should also be aware that every file can be read by Python but the result may not always be what you expect. For now lets use only text files.

To create a text file (call it ``text_file``) using the same program with which you write python scripts and add the following content::

    1
    2
    3
    a
    b
    c

Reading
-------

In order to **read** a file you first need to ``open`` it, like so::

    >>> f = open('text_file', 'r')

This line is telling python to open a file named ``text_file`` in **r**\ ead mode.
You can then **read** it all in one go::

    >>> data = f.read()
    >>> print(data)
    '1\n2\n3\na\nb\nc'

And in the end, if you are done with the file you should always **close** it::

    >>> f.close()

.. note::

    Keep in mind that as you read a file, python will keep track of what was read. If it reaches the end of the file, the next time you try to read from the same file you will get no lines. The solution to this is to simply ``f.close()`` and ``open()`` the file again.

An alternative mode of reading a file is to loop over it which will read one line at a time::

    >>> f = open('text_file', 'r')
    >>> for line in f:
    ...     print(line)
    ...
    1

    2

    3

    a

    b

    c
    >>> f.close()

Notice that there's an extra line between each line.
This happens because when reading a file, the line break present in the text file is kept by python.
If instead of printing the line you take a look at its representation (using the function ``repr``) you get::

    >>> f = open('text_file', 'r')
    >>> for line in f:
    ...     print(repr(line))
    ...
    '1\n'
    '2\n'
    '3\n'
    'a\n'
    'b\n'
    'c'
    >>> f.close()

So here you see that python uses the special ``\n`` character to represent line breaks.
If you want to get rid of it you can use the method ``.rstrip`` to remove characters from the right side of the string::

    >>> f = open('text_file', 'r')
    >>> for line in f:
    ...     print(repr(line.rstrip('\n')))
    ...
    '1'
    '2'
    '3'
    'a'
    'b'
    'c'
    >>> f.close()

Writing
-------

If instead you want to **write** to a file you need to need to ``open`` it in **w**\ rite mode, like so::

    >>> f = open('text_file', 'w')

And then **write** to it::

    >>> f.write("Some text")

Once you are done simply close the file::

    >>> f.close()

When writing to a file python writes in chunks of a certain size.
This will cause the actual write operation to be delayed until enough data is ready to be written or until the file is closed.
If however you don't want to close but want to **force** that all is written you can use::

    >>> f.flush()
