.. Python Club documentation master file, created by
   sphinx-quickstart on Fri Sep  7 18:32:12 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Club sessions
=============

.. toctree::
    :maxdepth: 2
    :glob:
    :numbered:

    */doc/index
