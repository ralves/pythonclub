.. Python Club documentation master file, created by
   sphinx-quickstart on Fri Sep  7 18:32:12 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Project
=======

The project is a long exercise with multiple blocks that span several weeks.
For it to make sense and be useful its parts should be performed in the
indicated order.

Later parts will always have as requirements, blocks from previous parts.

.. note::
    Contrary to exercises, no solutions will be provided for download.

.. toctree::
    :maxdepth: 2
    :glob:
    :numbered:

    *-project/doc/index
