My Boss Hates Me
================

Purpose
-------

Learn where classes might be helpful to reduce the amount of work
involved in adding extra functionality to existing code.

Problem
-------

Your boss loves you (and not just a little bit). Your latest set of experiments
look really promising, and a nature publication is just one experiment away.
The growth curves of a novel bacterial species you have been studying are sure
to somehow result in the cure for cancer, you can trust your boss on this one.

After a brief discussion, your boss wants you to create a plot showing the
average growth rate over 5 experiments. You head off, and write some awesome
python code that does just that. (See the data below).

At your next meeting, you proudly show your latest results to your boss... who
looks confused: "Why did you plot the mean alone, that makes no sense! You
should have made a plot of the mean, as well as the minimum!".... 

"%$&£ me !!!!!!" you say in your head "Well, its good that I decided to take
the time to write a class the first time around. With a bit of luck, making
these extra plots should take no time at all. And, just in case, its probably
also a good idea to create a third plot containing the maximum values. You
never know what will come next..."

Data
----
The :download:`data file <../data/data>` contains 100 numbers (separated by a
space) representing 5 experiments with 20 timepoints each. In the first 
plot you calculated the mean at each timepoint over all 5 experiments.

Now, you will have to use the same data to create 3 plots; the mean,
the minimum, and the maximum.

The file that *you* wrote can be :download:`downloaded here
<../data/my_nature_paper.py>`. (You should put it in the folder
which has the data file).
Take some time have have a look at how it works, and how the
individual functions that have to be performed are part of the class.

You can run the code from the command line::

    python my_nature_paper.py

and the plot will be created in the same folder.

Now, try to adapt the class to produce your new series of plots. You will
have to define some new functions (called methods when inside a class), and if
you find it helpful, add a couple of class attributes (variables belonging to
the class).

Try to repeat as little code as possible.

Hints
-----

When generating the plots, use a different filename for each plot which
reflects the method being plotted (mean/min/max).

Also adjust the title accordingly.

Extra
-----

Think what you would have to write if you had started with a function and then
had to modify it to achieve what is asked.

Member solutions
----------------

* Renato Alves - :download:`link <../solutions/rjalves.py>`
* Alexandre Leitao - :download:`link <../solutions/aleitao.py>`
