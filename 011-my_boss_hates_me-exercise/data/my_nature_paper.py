# -*- coding: utf-8 -*-

from __future__ import print_function, division
import os
import numpy as np
import matplotlib.pyplot as plt


class MyStatistics(object):
    def __init__(self, infile):
        self.infile = infile
        self.outfile = "myplot"

    def read_data(self):
        """Read data from the file self.infile, and store it in self.data.
        """
        data = np.fromfile(self.infile, sep=" ")
        self.data = data.reshape((5, 20))

    def mean(self):
        """Calculate the mean over all experiments (rows) in self.data, and
        return the result
        """
        return np.mean(self.data, 0)

    def make_plot(self):
        """Create and show a plot of something.
        """
        xvalues = range(self.data.shape[1])
        yvalues = self.mean()

        plt.title("mean")
        plt.plot(xvalues, yvalues)
        plt.savefig(self.outfile + ".png")
        plt.close()


def main():
    mystats = MyStatistics('data')
    mystats.read_data()
    mystats.make_plot()


if __name__ == "__main__":
    main()

# vim: ai sts=4 et sw=4
