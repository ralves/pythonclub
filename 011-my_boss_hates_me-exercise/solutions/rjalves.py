# -*- coding: utf-8 -*-

from __future__ import print_function, division
import os
import numpy as np
import matplotlib.pyplot as plt


class MyStatistics(object):
    def __init__(self, infile):
        self.infile = infile
        self.outfile = "myplot"
        self.method = ""

    def read_data(self):
        """Read data from the file self.infile, and store it in self.data.
        """
        data = np.fromfile(self.infile, sep=" ")
        self.data = data.reshape((5, 20))

    def mean(self):
        """Calculate the mean over all experiments (rows) in self.data, and
        set it as self.yvalues.
        In addition set the current method used to self.method
        """
        self.yvalues = np.mean(self.data, 0)
        self.method = "mean"

    def min(self):
        """Calculate the min over all experiments (rows) in self.data, and
        set it as self.yvalues.
        In addition set the current method used to self.method
        """
        self.yvalues = np.min(self.data, 0)
        self.method = "min"

    def max(self):
        """Calculate the max over all experiments (rows) in self.data, and
        set it as self.yvalues.
        In addition set the current method used to self.method
        """
        self.yvalues = np.max(self.data, 0)
        self.method = "max"

    def make_plot(self):
        """Create and show a plot of something.
        """
        xvalues = range(self.data.shape[1])

        plt.title(self.method)
        plt.plot(xvalues, self.yvalues)
        plt.savefig(self.outfile + "_" + self.method + ".png")
        plt.close()


def main():
    mystats = MyStatistics('data/data')
    mystats.read_data()

    mystats.mean()
    mystats.make_plot()

    mystats.min()
    mystats.make_plot()

    mystats.max()
    mystats.make_plot()

if __name__ == "__main__":
    main()

# vim: ai sts=4 et sw=4
