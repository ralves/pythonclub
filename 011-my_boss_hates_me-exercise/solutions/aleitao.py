
# -*- coding: utf-8 -*-

from __future__ import print_function, division
import os
import numpy as np
import matplotlib.pyplot as plt


class MyStatistics(object):
    def __init__(self, infile):
        self.infile = infile		#infile is the object that you called form MyStatistics('filepath') 
        self.outfile = "myplot"

    def read_data(self):
        """Read data from the file self.infile, and store it in self.data.
        """
        data = np.fromfile(self.infile, sep=" ")
        self.data = data.reshape((5, 20))

    def mean(self):
        """Calculate the mean over all experiments (rows) in self.data, and
        return the result
        """
        self.outfile = 'mean'
        return np.mean(self.data, 0)

    def minimum(self):
        '''Calculate the minimum over all experiments (rows) in self.data, and returns the result'''
        self.outfile = 'min'
        return np.min(self.data, 0)

    def maximum(self):
        self.outfile = 'max'
        return np.max(self.data, 0) 

    def make_plot(self):
        """Create and show a plot of something.
        """
        parameter_list = [self.mean, self.minimum, self.maximum]
        xvalues = range(self.data.shape[1])

        for parameter in parameter_list:
            yvalues = parameter()
            plt.title(self.outfile)
            plt.plot(xvalues, yvalues)
            plt.savefig(self.outfile + ".png")
            plt.close()


def main():
    mystats = MyStatistics('C:\Python27\pythonclub\classes\data1.txt')
    mystats.read_data()
    mystats.make_plot()


if __name__ == "__main__":
    main()

# vim: ai sts=4 et sw=4
