from __future__ import division 
import matplotlib
matplotlib.use('Agg')
import numpy as np

def boltzman(x, xmid, tau):
    """
    evaluate the boltzman function with midpoint xmid and time constant tau
    over x
    """
    return 1. / (1. + np.exp(-(x-xmid)/tau))


def main():

    # :)
    np.random.seed = 4

    hrs = 20
    days = 5
    data = np.zeros((days,hrs))

    for i in range(days):
        x = np.arange(0, hrs,)
        r = (np.random.rand(hrs) - 0.5)/10
        s = boltzman(x, hrs/2, 1)
        s += r
        s = abs(s)
        s = s - min(s)
        s = s / max(s)
        s = sorted(s)
        data[i] = s

    data.tofile("data/data", sep=" ")


if __name__ == '__main__':
    main()
