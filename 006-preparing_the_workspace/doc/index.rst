Preparing the workspace
=======================

Virtualenv
----------

A **Virtualenv** is a tool to create isolated python environments with different versions of modules.
These isolated environments called Virtual Environments (or virtualenvs) for short).

It can be particularly useful in three scenarios:

* In a system on which you don't have access to the Package Manager (mostly on linux) or don't have administrative privileges.
* If a given module is already installed on the system but you require a different version of it (or multiple versions for testing).
* If you wish to keep all the relevant modules for a given project in one folder.

.. note::
    In this document the term **package** is used to refer both to collections of Python **modules** and to applications installable via the Package Manager.

Installing
^^^^^^^^^^

To create a virtualenv you first need to install it.

On our Virtual Linux machine if you try to execute the command virtualenv it will tell you that it's not installed and will inform you of which command you need to run to install it::

    sudo apt-get install python-virtualenv

.. note::
    Because apt-get requires administrative privileges you will need to add the prefix **sudo**.
    When prompted for a password use **pythonclub**.

If you are not using our Linux machine you can download and install virtualenv following the instructions from `the official website <http://www.virtualenv.org/en/latest/#installation>`_.

How to use
^^^^^^^^^^

Once installed you can create a virtualenv. In this case we will call our virtualenv **venvtest**.

First go into the location where you would like to store your virtualenv and then run::

    virtualenv venvtest

After a few seconds a folder called **venvtest** should exist in your current location.

To use it simply run::

    source venvtest/bin/activate

You will have a visual clue that the virtualenv is active. When active your shell prompt will look something like::

    (venvtest)pythonclub@xubuxubu:

Distinction about isolated Virtualenvs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Before moving forward there's 2 variations of a virtualenv that you should be aware.

At creation you can specify that your virtualenv should be **isolated** or instead include all packages already present on the system.

In older versions of virtualenv the default was to create a **non-isolated** environment. Newer versions now default to **isolated**.

An **isolated** virtualenv can be useful if you want control over all the modules available to your script, for instance when figuring out why a certain script gives different results or to prevent your project from breaking if a module is updated on the system.
Last point being specially relevant in shared systems (like the IGC computers).

You can explicitly ask for creation of an **isolated** environment by using the command::

    virtualenv --system-site-packages venvtest

or **non-isolated** with::

    virtualenv --no-site-packages venvtest

You can additionally check if an existing virtualenv is isolated by looking for the file **venvtest/lib/pythonX.X/no-global-site-packages.txt** (where X.X is the python version used to create the virtualenv).

Installing packages/modules
---------------------------

Once a virtualenv is in place we now proceed to install things on it.

The central place to look for python packages/modules is `PyPi, the Python Package Index <http://pypi.python.org/pypi>`_. Google being your second best option.

If a package is written entirely in Python installing it is as easy as running::

    pip install package_name

and if no errors show up the package is installed.

However some Python packages depend on extra libraries written in other programming languages giving errors when these are not found.

We'll guide you through the installation of the package **biopython**, a suite of tools to do bioinformatics analysis and teach you two approaches to deal with the most common errors.

This is achieved by using the command (you need internet access for this command to work)::

    pip install biopython

.. note::
    If you find a module that includes as part of the installation the command **sudo make install** or **sudo python setup.py install**, create a virtualenv first and then run the command without including the **sudo** prefix.

    Using these commands with **sudo** is considered a bad practice because they don't provide a **uninstall** mechanism and there's no simple way of knowing beforehand where files are going to be stored.

When things go wrong
^^^^^^^^^^^^^^^^^^^^

If you tried to install biopython using **pip** on our PythonClub Linux machine you probably got an error saying something is missing.

Take your time to read the last lines of output, usually the error tells you what is wrong.

At any point, if you find things which you don't know, Googling about it is your best option.

Another module is missing
^^^^^^^^^^^^^^^^^^^^^^^^^

In this error there's a message saying a package called **NumPy** couldn't be found.

**NumPy** is an extremely useful numerical tookit and is great for matrix calculation and number crunching.
Due to its heavy computational nature it's written in *C* (another programming language which is faster but less user friendly) and relies on other highly specialized *C* numerical libraries.
Depending on how the *C* code was written, installing such modules may be non-trivial.

.. note::
    For example purposes we consider numpy a *hard to install* package.

In these situations one can look for a 'ready-to-use' version of the module/package (which automatically retrieves all dependencies) in Ubuntu Package Manager (called apt). The package for NumPy is called **python-numpy**.

.. note::
    If we want to use this version (which is installed **system-wide**) we can **no longer** use an **isolated** virtualenv.

To install NumPy on the system via the Package Manager using the command apt-get, run::

    sudo apt-get install python-numpy

Then to change the virtualenv to be **non-isolated** run::

    virtualenv --system-site-packages venvtest

.. note::
    If you wish to still use an isolated virtualenv you can run **pip install numpy**. For optimized performance you should install the *Atlas* and *Lapack* libraries.

A file is missing or couldn't be found
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Now that NumPy is installed we can try to install biopython again::

    pip install biopython

we no longer get a NumPy error but we get a different, uglier error.

Reading the last lines we see the error *'Python.h' No such file or directory*. This basically means **Python.h** couldn't be found.

Assuming that this file is part of some package available via the Package Manager we can search for the package (or packages) that contain it.

To do so head over to `Ubuntu Packages Search <http://packages.ubuntu.com/>`_ and scroll down to the section **Search the contents of packages**.

In keyword type *Python.h*, select **packages that contain files named like this**, selected your Ubuntu distribution (the one used for the PythonClub Virtual Machine is *Precise*) and press *Search*.

If no results appear try selecting the *...-updates* or *...-backports* alternatives. If still no results appear it means no package contains the specified file.
If this happens the only alternative is to use a more generic search engine like Google. Hopefully you won't need to come to this.

More frequently than not you will be presented with multiple results.
In this case you can try to install packages starting with the one that best matches your requirements and retry the installation until that particular error is no longer.

.. note::
    In Ubuntu and other Linux distributions, it's common practice to separate development files from 'ready-to-use' applications.
    For a given application called **python** its development files will likely be found in **python-dev**. The suffix **-dev** identifies development packages.

Specifically, for our current problem, the best matching package is **python2.7-dev** because we are using Python 2.7. So we should now run::

    sudo apt-get install python2.7-dev

and try **biopython** again with::

    pip install biopython

which should now finish with success.

.. note::
    By solving a problem with a particular package you will also fix future packages which depend on the same files. This means that with time you will have less errors when installing new packages.

Using modules
-------------

In the previous session we've mentioned the expressions::

    >>> from ... import ...
    >>> import ...

for reusing functions that you have written in some files.

Using third-party modules is no different, but first you need to know *what* functionality to import.

**BioPython** developers do a great job keeping an `online Tutorial/Cookbook <http://biopython.org/DIST/docs/tutorial/Tutorial.html>`_ with usage examples of most functionality present in the package.

We'll go over the steps of retrieving a protein sequence from Uniprot (a protein database) via ExPASy (a portal to multiple bioinformatics resources) and using NCBI's BLAST to search for similar proteins.

Retrieving a sequence based on ID
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

First we retrieve the sequence of our favorite protein **Myc** in *Mus musculus* which in `Uniprot <http://www.uniprot.org/>`_ has the ID **P01108**.

From the Biopython tutorial we learn that BioPython's package is actually called **Bio** and that the functionality we want is located in the ExPASy submodule.

We also learn that **retrieving** the entry and **reading** it into a usable format are two independent steps::

    >>> from Bio import ExPASy                    # For retrieving the entry
    >>> from Bio import SeqIO                     # For reading the output into something usable
    >>> handle = ExPASy.get_sprot_raw('P01108')   # 'handle' here is similar to what you get when you open() a file
    >>> seq_record = SeqIO.read(handle, 'swiss')  # We need to tell SeqIO that the handle will be in SwissProt format 'swiss'
    >>> handle.close()                            # Just like with files we should close it when finished

The variable **seq_record** contains a BioPython object called SeqRecord which is used to represent an entry.

If you do::

    >>> print(seq_record)

you will see a bunch of information which is part of **Myc**'s entry.

Extracting the sequence from the output
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Now that we have a SeqRecord we have everything we need to extract the sequence.

We read on the information about the `SeqRecord object <http://biopython.org/DIST/docs/tutorial/Tutorial.html#htoc32>`_ that it contains an attribute **.seq** which is itself a `Seq object <http://biopython.org/DIST/docs/tutorial/Tutorial.html#htoc16>`_.

To extract the sequence as a string we only need to do::

    >>> seq = seq_record.seq.tostring()

We can confirm this by doing::

    >>> print seq
    MPLNVNFTNRNYDLDYDSVQPYFICDEEENFYHQQQQSELQPPAPSEDIWKKFELLPTPPLSPSRRSGLCSPSYVAVATSFSPREDDDGGGGNFSTADQLEMMTELLGGDMVNQSFICDPDDETFIKNIIIQDCMWSGFSAAAKLVSEKLASYQAARKDSTSLSPARGHSVCSTSSLYLQDLTAAASECIDPSVVFPYPLNDSSSPKSCTSSDSTAFSPSSDSLLSSESSPRASPEPLVLHEETPPTTSSDSEEEQEDEEEIDVVSVEKRQTPAKRSESGSSPSRGHSKPPHSPLVLKRCHVSTHQHNYAAPPSTRKDYPAAKRAKLDSGRVLKQISNNRKCSSPRSSDTEENDKRRTHNVLERQRRNELKRSFFALRDQIPELENNEKAPKVVILKKATAYILSIQADEHKLTSEKDLLRKRREQLKHKLEQLRNSGA

Submitting sequence against NCBI BLAST
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

One more search on the Tutorial and we find how to `Run BLAST over the Internet <http://biopython.org/DIST/docs/tutorial/Tutorial.html#htoc84>`_.

We also find out that we should use a **Fasta** formatted sequence so we go back and do::

    >>> seq = seq_record.format('fasta')

which now includes the ">P01108 ..." header characteristic of Fasta files.

Then to run the BLAST we do::

    >>> from Bio.Blast import NCBIWWW                 # For running remote BLAST
    >>> handle = NCBIWWW.qblast("blastp", "nr", seq)  # "blastp" because we are using a protein sequence and "nr" because we want to use the Non-Redundant Protein Database

And after some seconds we have results.

Reading BLAST results
^^^^^^^^^^^^^^^^^^^^^

Once again the Tutorial tells us how to `Parse BLAST output <http://biopython.org/DIST/docs/tutorial/Tutorial.html#htoc90>`_::

    >>> from Bio.Blast import NCBIXML
    >>> result = NCBIXML.read(handle)

And finally how to work with the `Blast result object <http://biopython.org/DIST/docs/tutorial/Tutorial.html#htoc91>`_::

    >>> print len(result.alignments)                     # We have 50 hits which is the default if no parameters are specified
    50
    >>> print result.alignments[0].hit_def               # Our first hit is the Myc protein from Mus musculus (isoform c)
    myc proto-oncogene protein isoform c [Mus musculus]
    >>> print result.alignments[0].accession             # The unique ID from NCBI's GenPept (isoform c)
    NP_001170824
    >>> print result.alignments[1].hit_def               # Second hit is Myc (isoform b)
    myc proto-oncogene protein isoform b [Mus musculus]
    >>> print result.alignments[1].accession             # The unique ID from NCBI's GenPept (isoform b)
    NP_001170823
    >>> print result.alignments[2].hit_def               # Third hit is Myc (isoform a)
    myc proto-oncogene protein isoform a [Mus musculus]
    >>> print result.alignments[2].accession             # The unique ID from NCBI's GenPept (isoform a)
    NP_034979
    ...

From here onwards it's up to you what to do next.

.. note::
    Here we rely on the annotation provided by NCBI. Keep in mind that sometimes this information is incomplete, abiguous or in some not so frequent cases even wrong.

Extra
-----

IPython
^^^^^^^

When running interactive Python sessions with modules you aren't familiar with it's helpful to have some extra functionality to simplify exploration.

I won't cover the details here but for future reference `IPython <http://ipython.org/>`_ provides a rich environment for interactive code execution and data visualization.

You can install it via::

    sudo apt-get install ipython

More on Virtualenvs and pip
^^^^^^^^^^^^^^^^^^^^^^^^^^^

You can find more information about virtualenv and pip on the `old experimental PythonClub page </old/001-virtualenv/doc/index.html>`_.
