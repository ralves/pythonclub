.. _matplotlib-installation:

2D Plotting
===========

In this session we will introduce you to the Matplotlib toolkit, the most popular solution for 2D plotting with Python.

First we start by creating and activating an isolated virtualenv::

    virtualenv --no-site-packages pyclubvenv
    source pyclubvenv/bin/activate

Numpy
-----

Matplotlib depends on numpy so we need to install it first.

.. note::
    If you installed numpy on your PythonClub machine as part of session 6, please remove it with **sudo apt-get purge python-numpy** before proceeding.

.. note::
    Dependencies (need to be installed via **sudo apt-get**): python-dev gfortran liblapack-dev libatlas-base-dev

Once all dependencies are in place you can simply run::

    pip install numpy

Matplotlib
----------

.. note::
    Dependencies: g++ libfreetype6-dev libpng12-dev python-tk tk-dev
    
Again once all dependencies are in place we run::

    pip install matplotlib

Pyplot vs Pylab
^^^^^^^^^^^^^^^^

**Pyplot** is a collection of functions designed to simplify the use of matplotlib. Users familiar with MATLAB will find this interface more intuitive.

If the plot you wish to achieve goes beyond the provided built-in functions, you will need to dive into matplotlib's internals or look in the `Gallery <http://matplotlib.org/gallery.html>`_ for examples/inspiration.

**Pylab** is a shortcut that merges Pyplot and numpy in the same namespace. Although indicated for interactive sessions, we do not recommended it.

Therefore the recommended way of importing pyplot is::

    >>> import numpy as np
    >>> import matplotlib.pyplot as plt

Plotting
--------

We provide some examples of the most frequent plot types.

Histogram
^^^^^^^^^

A simple histogram of a beta distribution:

.. plot::
    :include-source:

    from __future__ import division
    import numpy as np
    import matplotlib.pyplot as plt

    a = 2
    b = 5
    samples = 50000
    bins = 50

    plt.hist(np.random.beta(a, b, samples), bins)
    plt.grid()
    plt.title(r'Beta distribution - $\alpha$={0}, $\beta$={1}, samples={2}, binning={3}'.format(a, b, samples, 1/bins))

    plt.show()

Bar plot
^^^^^^^^
A colorful bar plot of DNA frequencies:

.. plot::
    :include-source:

    from __future__ import division
    import numpy as np
    import matplotlib.pyplot as plt

    def compute_frequency(seq):
        counts = {}
        values = []
        size = len(seq)

        for nucleotide in seq:
            if nucleotide not in counts:
                counts[nucleotide] = 1
            else:
                counts[nucleotide] += 1

        for nucleotide in xlabels:
            values.append(counts[nucleotide] / size)

        return values

    filename = "../../004-the_central_dogma-exercise/data/DNA"
    xlabels = ('A', 'T', 'C', 'G')
    width = 0.3                          # bar width
    xcorr = 0.05                         # small ajustment for centering bars
    x = np.arange(len(xlabels)) + xcorr  # used for bar positioning
    
    with open(filename) as fh:
        # Sequence 1
        seq1 = fh.readline().rstrip()
        values = compute_frequency(seq1)
        p1 = plt.bar(x, values, width, color='#FF7575')

        # Sequence 2
        seq2 = fh.readline().rstrip()
        values = compute_frequency(seq2)
        p2 = plt.bar(x + width, values, width, color='#2FAACE')

    # Average across all sequences
    with open(filename) as fh:
        all_values = []

        for seq in fh:
            all_values.append(compute_frequency(seq.rstrip()))

        total_seqs = len(all_values)
        values_array = np.array(all_values)
        values = []
        stderror = []

        for i in x:
            data = values_array[:,i]  # counts across all sequences for given aminoacid
            values.append(np.mean(data))
            stderror.append(np.std(data))

        p3 = plt.bar(x + 2 * width, values, width, yerr=stderror, color='#DFE32D')

    plt.title("Nucleotide frequency in DNA from exercise 004")
    plt.xticks(x + 3/2 * width, xlabels)
    plt.xlabel("Nucleotide")
    plt.ylabel("Frequency")

    plt.legend((p1[0], p2[0], p3[0]), ('Sequence 1', 'Sequence 2', 'All sequences'), loc='lower right')

    plt.show()

Scatter plot
^^^^^^^^^^^^

A scatter plot of randomized normal noise:

.. plot::
    :include-source:

    from __future__ import division
    import numpy as np
    import matplotlib.pyplot as plt

    xrand = np.random.randn(2000)
    yrand = np.random.randn(2000)
    
    plt.plot(xrand, yrand, 'b.')
    plt.title("Randomized normal noise")
    plt.xlim(-4, 4)
    plt.ylim(-4, 4)

    plt.show()

Box plot
^^^^^^^^

A box plot of different randomized distributions:

.. plot::
    :include-source:

    from __future__ import division
    import numpy as np
    import matplotlib.pyplot as plt

    normal = np.random.randn(2000)
    laplace = np.random.laplace(0, 0.5, 2000)
    gamma = np.random.gamma(1, 0.5, 2000)

    plt.boxplot((normal, laplace, gamma))
    plt.title("Normal vs Laplace vs Gamma")
    locs, labels = plt.xticks()
    plt.xticks(locs, ('Normal', 'Laplace', 'Gamma'))

    plt.show()
