Plot my Math
============

Learning how to do efficient mathematical operations on large matrices.

Purpose
-------

Explore basic usage of numpy arrays, some mathematical functions and a little more on plotting.

Problem
-------

You have a file containing data which you want to plot.
But, like most automatically generated data, it's completely useless as-is;
you'll have to manipulate it before its useful.

.. note::
    Remember that python starts counting at zero, so in this exercise when we say row 3 we mean mylist[3], which also means there is a row 0 corresponding to mylist[0].

.. note::
    Also remember than when you perform a slice and you want to get rows 1, 2 and 3 you need to do 1:4.
    If you want to retrieve a section of a matrix (2 dimensional slice) between coordinates (xA, yA) and (xB, yB) you need to do [xA:xB+1, yA:yB+1]

You will need to do the following actions to get the expected output:

1. Read and reshape the data (as shown in hints)
2. Transpose the matrix
3. Apply a log10 transformation to all values
4. Copy row 11 to 15 and 18
5. Copy column 8 to columns 9 to 11 (use the slice syntax here)
6. Transform all numbers to positive
7. Multiply all numbers between coordinates (2,3) and (18,7) by 20
8. Display the final result as a heatmap. It should be obvious if you got it right :P .

Data
----
The :download:`data file <../data/data>` contains 400 numbers (separated by a space) representing a 20x20 matrix.

.. note:: 
    You will need the **virtualenv** you setup before (see :ref:`matplotlib-installation` for instructions)

Hints
-----

You can read the contents of the file by::

    >>> import numpy
    >>> data = numpy.fromfile('data', sep=' ')

You can view the data as a 'heatmap'

    >>> import matplotlib.pyplot as plt
    >>> data_reshaped = data.reshape((20, 20))
    >>> plt.pcolor(data_reshaped)
    >>> plt.show()

.. note::
    To learn a little about numpy's array manipulation syntax you can look into the `Tentative Numpy Tutorial <http://scipy.org/Tentative_NumPy_Tutorial#head-864862d3f2bb4c32f04260fac61eb4ef34788c4c>`_ (scroll up and down on that page for a lot more on **numpy** if you are interested).

Member solutions
----------------

* Renato Alves and Marc Gouw - :download:`link <../solutions/rjalves_and_mgouw.py>`
