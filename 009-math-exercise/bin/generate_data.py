# -*- coding: utf-8 -*-

from __future__ import print_function, division
import numpy


def main():
    data = numpy.fromfile("data/expected", sep=" ")
    data = data.reshape((20, 20))
    data[2:19,3:8] = data[2:19,3:8] / 20
    negatives = numpy.random.rand(20, 20)
    negatives[negatives<=0.5] = -1
    negatives[negatives>0.5] = 1
    data = data * negatives
    data[:,9:12] = (numpy.random.rand(20,3) - 0.5)
    data[(15,18),:] = (numpy.random.rand(2,20) - 0.5)
    data = 10**data
    data = data.transpose()
    data.tofile("data/data", sep=" ")


if __name__ == "__main__":
    main()

# vim: ai sts=4 et sw=4
