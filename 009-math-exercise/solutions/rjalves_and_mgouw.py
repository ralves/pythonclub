# -*- coding: utf-8 -*-

from __future__ import print_function, division
import numpy
import matplotlib.pyplot as plt


def main():
    # Read data as numpy array
    data = numpy.fromfile("data/data", sep=" ")

    # Turn the array into a 20 by 20 matrix
    data = data.reshape((20, 20))

    # Transpose the matrix
    data = data.transpose()

    # Apply log10 transformation to data
    data = numpy.log10(data)

    # Copy the content of line 11 to both line 15 and 18
    # NOTE: array[(15, 18), :] means give me lines 15 and 18 and ", :" means
    # give me all numbers in these lines.
    # NOTE: (11, 11) means give me a 2 line array where both line 1 and line 2
    # have the content of line 11 in the matrix
    data[(15, 18), :] = data[(11, 11), :]

    # Copy the content of column 8 to lines 9, 10 and 11
    # NOTE: Remember 9:12 reads as 9 till 12 (excluding 12)
    # NOTE: Remember (8,) is a tuple, if multiplied by 3 you get (8, 8, 8)
    data[:, 9:12] = data[:, (8,) * 3]

    # Turn all negative numbers to positives
    data = numpy.abs(data)

    # Retrieve a subset of the matrix, multiply it by 20 and reassign it back
    data[2:19, 3:8] = data[2:19, 3:8] * 20

    # Plot a heatmap of the data
    plt.pcolor(data)
    plt.show()


if __name__ == "__main__":
    main()

# vim: ai sts=4 et sw=4
