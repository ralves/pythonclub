**********************
PythonClub conventions
**********************

This repository holds all exercises and data used for Python Club sessions.


Naming lectures and exercises
=============================

*Lectures* should have a **title** and be organized in folders using a
**zero-padded number prefix**::

   001-basics/

*Exercises*, in addition to the conventions used in *Lectures* you should also append the suffix **-exercise** to the folder name::

   002-can_I_cry_now-exercise/

Folder structure
================

Exercise folders should contain an *index.rst* file formatted according to
`reStructuredText <http://sphinx.pocoo.org/rest.html>`_ and `Sphinx's Markup
<http://sphinx.pocoo.org/markup/index.html>`_.

They should also contain the folders *bin*, *data*, *doc*, *output* following
the conventions:

* **doc/index.rst** - short description of the exercise its purpose and what should
  be produced (output)

* **bin/**       - python code and required modules for this exercise (everyone
  will also create their solutions here)

* **data/**      - data/content required to run the code (flat-files, databases,
  other formats)

* **doc/**       - additional documentation for modules or any used toolkit (PDF, text
  file, no Word documents)

* **output/**    - empty folder where all intermediate and final files will be
  written to (when needed)

* **solutions/** - empty folder where all accepted solutions will be available
  after the club

Git the source
==============

Before going forward you need to download and install `git <http://git-scm.com/>`_ for you system.

In order to gain access to the repository that includes all sessions and exercises you can execute::

    git clone git://pythonclub.igc.gulbenkian.pt:8080/pythonclub.git

this will grant you read access. If you wish to have write access to be able to add lectures or exercises you need to talk to us first in order to setup your access. After that you should use the next command instead::

    git clone git@pythonclub.igc.gulbenkian.pt:pythonclub.git
