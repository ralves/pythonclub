.. Python Club documentation master file, created by
   sphinx-quickstart on Fri Sep  7 18:32:12 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Python Club's documentation!
=======================================

.. note::
    To receive notifications when new content is added, login on the `IGC forum
    - Python Club thread <https://webint.igc.gulbenkian.pt/phpBB3/viewtopic.php?f=21&t=397>`_ 
    and click on the bottom-left link **Subscribe topic**.

Contents:

.. toctree::
    :maxdepth: 2
    :glob:

    readme
    club_sessions
    exercises
    project

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Old experimental content
========================

You can find the old experimental content in `this location </old/>`_.
