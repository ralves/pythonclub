Plot stains
===========

A typical meeting out of the lab ends up with coffee stains all over your lab notebook ruining some of your results.

Purpose
-------

In this exercise we will explore a few more functions in the BioPython and matplotlib packages.

Problem
-------

You performed a few analyses on your proteins, made a plot, printed it, glued it to your lab notebook and threw away the code.

A couple of weeks have passed and when discussing the results with someone else your plot ended up looking like this:

.. image:: coffee-plot.png

After the damage was done you went back to your notes trying to find enough information to **redo the plot** exactly as it looks on the original image.

Data
----

You were able to find the :download:`list of protein identifiers <../data/protein_ids>` that you used before (you can tell that they are Uniprot identifiers), and a reference to a **BioPython** module called **ProtParam.ProteinAnalysis**.

.. note::
    You will need to have a virtualenv setup with BioPython and Matplotlib installed.
    Please refer to :ref:`matplotlib-installation` for instructions.
    The order of installation of packages should be **numpy**, **matplotlib**, **biopython**.

.. warning::
    All the steps required to create the plot as it looks on the image were performed via programming.
    This includes going from Uniprot IDs to the Gene name. The coffee stain being the only exception ;) .

Hints
-----

In order to explore what functionality is available in a module (in the example **os**) use the function `dir() <http://docs.python.org/2/library/functions.html#dir>`_ like so::

    >>> import os
    >>> print(dir(os))

and look for anything that doesn't start with an underscore _ .

Optionally you can also use the module `pprint <http://docs.python.org/2/library/pprint.html#pprint.pprint>`_ to make output more readable like so::

    >>> import os
    >>> from pprint import pprint
    >>> pprint(dir(os))

Member solutions
----------------

For future reference, here will be added a few solutions for this exercise:

* Renato Alves - :download:`link <../solutions/rjalves.py>`
