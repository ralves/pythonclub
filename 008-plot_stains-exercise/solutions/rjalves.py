# -*- coding: utf-8 -*-

from __future__ import division
from Bio import SeqIO
from Bio import ExPASy
from Bio.SeqUtils import ProtParam
import matplotlib.pyplot as plt


def retrieve_sequences():
    """
    Reads the protein_ids file, returning an iterator of SeqRecord objects
    """
    with open("data/protein_ids") as fh:
        for line in fh:
            protein_id = line.rstrip()
            handle = ExPASy.get_sprot_raw(protein_id)
            seq_record = SeqIO.read(handle, "swiss")
            yield seq_record


def compute_measures(record_list):
    """
    Using a list (or iterator) containing SeqRecord objects, calculates and
    returns the instability index and isoelectric_point of each sequence
    """
    for seq_record in record_list:
        params = ProtParam.ProteinAnalysis(str(seq_record.seq))
        instability = params.instability_index()
        iso_elect = params.isoelectric_point()
        yield seq_record, instability, iso_elect


def make_plot(data):
    width = 0.4
    ylabels = []
    ylocation = []

    for i, results in enumerate(data):
        seq_record, insta, iso_elect = results

        ylocation.append(i + width)
        labels = seq_record.annotations["gene_name"].split(";")

        # If no gene name is found use the Uniprot ID
        label = seq_record.id

        for l in labels:
            if l.startswith("Name="):
                label = l[5:]
                break

        ylabels.append(label)

        p1 = plt.barh(i, insta, width, color="red")
        p2 = plt.barh(i + width, iso_elect, width, color="blue")

    plt.yticks(ylocation, ylabels)
    plt.title("Instability Index vs Isoelectric Point")

    # How to position the legend can be found on:
    # http://matplotlib.org/users/legend_guide.html
    plt.legend((p1, p2), ('Instability Index', 'Isoelectric Point'),
               bbox_to_anchor=(0, 0.1, 1, 1),
               loc=4)

    plt.savefig("output/rjalves.png")


def main():
    sequences = retrieve_sequences()
    results = compute_measures(sequences)
    make_plot(results)


if __name__ == "__main__":
    main()

# vim: ai sts=4 et sw=4
