:orphan:

Congratulations!
================

.. image:: centraldogma.jpg

The **Central Dogma** has often attacked for being wrong, but in the epic paper announcing the structure of DNA, Watson and Crick correctly proposed the molecular flow of information in biology.
Read their :download:`original publication here <centraldogma.pdf>`.
