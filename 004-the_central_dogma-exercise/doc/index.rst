The Central Dogma
=================

This exercise is all about the **Central Dogma** of molecular biology.
In this exercise, we will be using python to emulate the processes
of transcription and translation.

Purpose
-------

This exercise involves a more complicated application of the techniques
we covered so far. The main techniques
implemented are **string processing**, **file manipulation** for **reading
and writing**, storing information in **dictionaries**, and looking up information
in **lists**.

A little History
----------------
As you probably know, the **Central Dogma** of molecular biology dates back to the
1950's, when Watson, Crick and Franklin determined the molecular structure of
DNA. It is typically depicted as:

::

    DNA --> RNA --> Protein

Since its inception, there have been many attacks on the **Central Dogma**,
claiming that this simple model does not cover all possibilities in biology.
The solution of this exercise will help shine some light on this long debate.

Problem
-------

We will start with a file containing multiple :download:`DNA sequences <../data/DNA>`.
In the following two steps, we will **transcribe and translate** each of these sequences,
each time writing the new sequences to a new output file.

To save some time, we have included a list of codon --> amino acid mappings for
use in the translation step.

Transcription
^^^^^^^^^^^^^

The :download:`DNA sequence file<../data/DNA>`, contains 23 DNA sequences.
Each line in the file corresponds to a single coding sequence reading from
5' --> 3' (left to right).

Your first task is to **transcribe** each sequence, and write the mRNA sequences
to a new file (for example: **RNA**).

Translation
^^^^^^^^^^^

Now that we have the transcribed the DNA sequences, we will need an codon lookup
table to translate the mRNA. A copy of the :download:`codon translation <../data/tRNA>`
has been created, with the following format:

::

    GAC:V
    GGU:T
    ...

Where 'GGU' (5'->3') corresponds to the tRNA anti-codon resulting the amino acid 'T'. Think of it as
follows:

::

                  T
                  |
    tRNA:  5'....GGU....3'  
    mRNA:  3'....CCA....5'

Since we will be using this information to translate our mRNA sequences it will be useful to store
their information in a way which we can repeatedly and quickly find the corresponding amino
acid for each codon. Create a  **dictionary** in which each **key** is a codon, and the corresponding
**value** is the resulting amino acid. 

Create a text file (called **proteins**, for instance), and save the translated sequences
to this file.

Congratulations! You are now ready to test if your expressed proteins are correct!

.. _004-solutions:

Solutions
---------

To see if the transcription/translation happened properly, have a look at the 10th amino acid in each
sequence, and stick them together to form an un-spaced phrase. You should be able to recognize
if you have the correct answer. 

If you think you do, try your answer by going to the following link:
``004-the_central_dogma-exercise/doc/solution/<your_solution>.html`` replacing
the ``<your solution>`` part with the answer you found.

