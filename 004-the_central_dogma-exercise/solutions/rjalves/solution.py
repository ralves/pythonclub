# -*- coding: utf-8 -*-

from __future__ import print_function, division
from string import maketrans


class DNASequence(object):
    def __init__(self, seq):
        self.seq = seq

    def set_codon_table(self, codons):
        self.codons = codons

    def transcribe(self):
        return self.seq.replace('T', 'U')

    def translate(self):
        aminoacids = []
        for i in range(0, len(self.seq) - 3, 3):
            aminoacids.append(self.codons[self.seq[i:i+3]])

        return ''.join(aminoacids)


def read_codons():
    # tRNA is the anti-sense of codons so we need to convert and invert them
    # and because we will do direct conversion from DNA, we use T instead of U
    conversion = maketrans('AUCG', 'TAGC')

    codons = {}
    with open("data/tRNA") as fh:
        for line in fh:
            trna, aminoacid = line.rstrip().split(":")
            codons[trna.translate(conversion)[::-1]] = aminoacid

    return codons


def read_DNA():
    with open("data/DNA") as fh:
        for seq in fh:
            yield DNASequence(seq.rstrip())


def write_RNA(seq):
    with open("solutions/rjalves/RNA", 'a') as fh:
        fh.write("{0}\n".format(seq.transcribe()))


def write_protein(seq):
    with open("solutions/rjalves/Proteins", 'a') as fh:
        protein = seq.translate()
        fh.write("{0}\n".format(protein))

    return protein[9]


def main():
    seqs = read_DNA()
    solution = ''
    codons = read_codons()

    for seq in seqs:
        # Write to RNA file
        write_RNA(seq)

        # Write to Protein file
        seq.set_codon_table(codons)
        solution += write_protein(seq)

    print(solution)


if __name__ == "__main__":
    main()

# vim: ai sts=4 et sw=4
