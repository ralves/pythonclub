
DNA_file = open('data/DNA')
mRNA_file = open('data/mRNA','w')

for DNA in DNA_file:
    RNA = DNA.strip().replace('T','U')
    mRNA_file.write(RNA)
    mRNA_file.write('\n')

DNA_file.close()
mRNA_file.close()

tRNA_file = open('data/tRNA')

tRNAs = {}

for line in tRNA_file:
    line = line.strip()
    tRNA, AA = line.strip().split(':')
    tRNAs[tRNA] = AA


mRNA_file = open('data/mRNA')
protein_file = open('data/protein','w')

for mRNA in mRNA_file:
    mRNA = mRNA.strip()
    for i in range(0,len(mRNA),3):
        codon = mRNA[i:i+3]

        #build the anticodon
        anticodon = codon[::-1]
        anticodon = anticodon.replace('A','u')
        anticodon = anticodon.replace('G','c')
        anticodon = anticodon.replace('C','G')
        anticodon = anticodon.replace('U','a')
        anticodon = anticodon.upper()

        # lookup the corresponding codon
        protein_file.write(tRNAs[anticodon])

    protein_file.write('\n')
        
mRNA_file.close()
protein_file.close()

protein_file = open('data/protein')

solution = ''
for protein in protein_file:
    solution += protein[9]

print(solution)

link = 'https://pythonclub.igc.gulbenkian.pt/004-the_central_dogma-exercise/doc/solution/'
link += solution
link += '.html'

print(link)

protein_file.close()



