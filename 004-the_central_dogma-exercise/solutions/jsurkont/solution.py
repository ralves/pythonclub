# -*- coding: utf-8 -*-

from __future__ import print_function, division

def chunks(l, n):
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

def transcribe(dna):
    return dna.replace('T', 'U')

def complement(nucleicacid):
    code = {'A':'U', 'T':'A', 'C':'G', 'G':'C', 'U':'A'}
    return ''.join(code[x] for x in nucleicacid)

def translate(mrna):

    genetic_code = {}
    with open('data/tRNA', 'r') as trna:
        for line in trna:
            lspl = line.strip().split(':')
            genetic_code[lspl[0]] = lspl[1]

    return ''.join(genetic_code[complement(x)[::-1]] for x in chunks(mrna, 3))


def main():

    file_dna = open('data/DNA', 'r')
    file_rna = open('solutions/jsurkont/RNA', 'w')
    for seq in file_dna:
        file_rna.write(transcribe(seq.strip()) + '\n')
    file_dna.close()
    file_rna.close()

    file_rna = open('solutions/jsurkont/RNA', 'r')
    file_protein = open('solutions/jsurkont/Proteins', 'w')
    for seq in file_rna:
        prot = translate(seq.strip())
        file_protein.write(prot + '\n')
        print(prot[9],  end='')
    print()
    file_rna.close()
    file_protein.close()
    


if __name__ == "__main__":
    main()

# vim: ai sts=4 et sw=4
