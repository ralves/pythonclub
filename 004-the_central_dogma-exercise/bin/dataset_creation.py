# -*- coding: utf-8 -*-

from __future__ import print_function, division
from random import randint, choice, seed
from Bio import Seq, SeqUtils

SEED = 12 #ensure consistency in random sequence generation
seed(SEED)

POS = 9 # the 10th position will contain the AminoAcid letters for the final solutio
SOLUTION = 'THEYWERERIGHTALLTHETIME'

STOP_CODONS = ['GAT','AAT','AGT']

def DNA_from_AA(aa):
    """ Return a DNA codon that corresponds
    to the AminoAcid aa.
    """
    standard_table = SeqUtils.CodonTable.unambiguous_dna_by_name["Standard"]

    return standard_table.back_table[aa] 

def create_fasta(filepath,n):
    """ Generate a DNA file with n sequences and write
    to filepath.
    """

    standard_table = SeqUtils.CodonTable.unambiguous_dna_by_name["Standard"]

    codons = standard_table.forward_table.keys()

    for s in STOP_CODONS:
        if s in codons:
            codons.remove(s)

    o = open(filepath,'w')

    for aa in SOLUTION:
        _seq = 'ATG' # stop codon, DNA
        l = randint(25,35)
        for _l in range(l):
            if _l == POS-1:
                codon = DNA_from_AA(aa)
                if codon in STOP_CODONS:
                    print('fuck')
            else:
                codon = choice(codons)
            _seq+=codon

        o.write(_seq+'\n')
    o.close()

def create_trna(filepath):
    """ Create a 2 column text file with
    RNA to amino acid mappings.
    """

    o = open(filepath,'w')

    standard_table = SeqUtils.CodonTable.unambiguous_rna_by_name["Standard"]

    for codon, aa in standard_table.forward_table.items():
        anticodon = Seq.reverse_complement(codon).replace('T','U')
        o.write('%s:%s\n' % (anticodon,aa,))

    o.close()

def test_solution(fasta_path, trna_path):
    """
    Test to make sure we get the right solution,
    and print the solution to the screen.
    """

    f = open(fasta_path)
    r = open('data/rna','w')

    for line in f:

        dna = line.strip()

        rna = dna.replace('T','U')

        r.write('%s\n'%rna)

    f.close()
    r.close()

    t = open(trna_path)

    trna = {}
    for line in t:
        _line = line.strip().split(':')
        trna[_line[0]] = _line[1]
    
    r.close()

    r = open('data/rna')

    p = open('data/protein','w')

    proteins = []

    for line in r:
        protein = ''
        _line = line.strip()
        for i in range(0,len(_line)-2,3):
            codon = _line[i:i+3]
            _trna = Seq.reverse_complement(codon).replace('T','U')
            protein += trna[_trna]
        proteins.append(protein)
        p.write('%s\n'%protein)

    r.close()
    p.close()

    solution = ''
    for protein in proteins:
        solution += protein[POS]
    
    if solution  == SOLUTION:
        print('Solution is OK!')
    else:
        print('Something is WRONG!!!!!!')
        print('The proposed solution was:')
        print(solution)
    
def main():
    fasta_path = "data/DNA"
    trna_path = "data/tRNA"
    create_fasta(fasta_path,len(SOLUTION))
    create_trna(trna_path)
    test_solution(fasta_path,trna_path)

if __name__ == "__main__":
    main()

# vim: ai sts=4 et sw=4


